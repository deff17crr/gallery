App = new Marionette.Application()
App.addRegions(content : '.content')

App.on 'start', ->
  if Backbone.history
    Backbone.history.start()