App.module 'Albums.List', (List, App, Backbone, Marionette, $, _) ->
  List.Controller = listAlbums: ->

    fetchingContacts = App.request('album:entities')
    $.when(fetchingContacts).then (albums) ->
      CompositeView = new (App.Albums.CompositeView)(collection: albums)

      App.content.show CompositeView