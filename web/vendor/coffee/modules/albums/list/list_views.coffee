App.module 'Albums', (Albums, App, Backbone, Marionette, $, _) ->
  Albums.ModelView = Marionette.ItemView.extend(
    template: '#album-view'
    className: 'album'
  )

  Albums.CompositeView = Marionette.CollectionView.extend(
    childView: Albums.ModelView
    className: 'album-list'
  )