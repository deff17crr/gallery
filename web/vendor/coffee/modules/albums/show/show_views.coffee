App.module 'Albums', (Albums, App, Backbone, Marionette, $, _) ->
  Albums.AlbumLayout = Marionette.LayoutView.extend(
    template : '#album-view-layout',
    className: 'album-show'
    regions :
      albumInfo: '.album-info',
      imagesList: '.images-list'
  );

  Albums.AlbumInfoView = Marionette.ItemView.extend(
    template: '#album-info-view'
  )

  Albums.AlbumImagesView = Marionette.ItemView.extend(
    template: '#album-images-template'
  )