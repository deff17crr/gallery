App.module 'Albums.Show', (Show, App, Backbone, Marionette, $, _) ->
  Show.Controller = showAlbum: (id, page) ->
    LayoutView = new App.Albums.AlbumLayout()
    App.content.show LayoutView

    #render album info
    fetchingAlbum = App.request('album:entity', id)
    $.when(fetchingAlbum).then (album) ->
      AlbumInfoView = new App.Albums.AlbumInfoView(model: album)
      LayoutView.albumInfo.show AlbumInfoView

    #render images and pagination
    fetchingAlbums = App.request('image:entities', id, page)
    $.when(fetchingAlbums).then (model) ->
      AlbumImagesView = new App.Albums.AlbumImagesView(model: model)
      LayoutView.imagesList.show AlbumImagesView

      if model.get('total') > 10
          AlbumImagesView.$el.find('.pagination').pagination(
            items: model.get('total')
            itemsOnPage: 10
            currentPage: page
            hrefTextPrefix: "#album/"+id+"/page/"
          );