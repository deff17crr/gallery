App.module 'Nav', (Nav, App, Backbone, Marionette, $, _) ->
  Nav.Router = Mn.AppRouter.extend(
    appRoutes:
      '' : 'index'
      'album/:id' : 'album'
      'album/:id/page/:page' : 'album'
  )

  App.on 'before:start', ->
    new (Nav.Router)(
      controller:
        index: ->
          App.Albums.List.Controller.listAlbums()

        album: (id, page) ->
          App.Albums.Show.Controller.showAlbum id, page
    )
  return