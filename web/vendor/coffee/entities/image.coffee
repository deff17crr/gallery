App.module 'Entities', (Entities, App, Backbone, Marionette, $, _) ->
  App.reqres.setHandler 'image:entities', (id, page)->
    Entities.ImagesPaginationModel = Backbone.Model.extend(
      url: '/api/v1/albums/'+ id + '/images?page=' + page
    )
    contacts = new Entities.ImagesPaginationModel()

    deffer = $.Deferred()
    contacts.fetch
      success: (data) ->
        deffer.resolve data
        return
      error: (data) ->
        deffer.resolve data
        return
    deffer.promise()