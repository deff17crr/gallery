App.module 'Entities', (Entities, App, Backbone, Marionette, $, _) ->
  Entities.Album = Backbone.Model.extend(
    urlRoot: '/api/v1/albums'
  )
  Entities.AlbumsCollection = Backbone.Collection.extend(
    url: '/api/v1/albums'
    model: Entities.Album
  )
  App.reqres.setHandler 'album:entities', ()->
    albums = new (Entities.AlbumsCollection)
    deffer = $.Deferred()
    albums.fetch
      success: (data) ->
        deffer.resolve data
        return
      error: (data) ->
        deffer.resolve data
        return
    deffer.promise()

  App.reqres.setHandler 'album:entity', (id)->
    album = new Entities.Album(
      id : id
    )
    deffer = $.Deferred()
    album.fetch
      success: (data) ->
        deffer.resolve data
        return
      error: (data) ->
        deffer.resolve data
        return
    deffer.promise()