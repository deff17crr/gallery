var gulp = require('gulp');
var compass = require('gulp-compass');
var path = require('path');
var coffee = require('gulp-coffee');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var merge  = require('merge-stream');
var less   = require('gulp-less');
var cleanCSS = require('gulp-clean-css');
var rebaseUrls = require('gulp-css-rebase-urls');

gulp.task('default', function() {
    custom_assets();
});

gulp.task('all', function() {
    custom_assets();

    gulp.src([
        './web/vendor/libs/jquery.js',
        './web/vendor/libs/underscore.js',
        './web/vendor/libs/backbone.js',
        './web/vendor/libs/backbone.marionette.min.js',
        './web/vendor/libs/fancybox/jquery.mousewheel-3.0.4.pack.js',
        './web/vendor/libs/fancybox/jquery.fancybox.js',
        './web/vendor/libs/simplePagination.js'
    ])
        .pipe(uglify())
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('./web/assets'));

    gulp.src([
        './web/vendor/libs/fancybox/jquery.fancybox.css'
    ])
        .pipe(cleanCSS())
        .pipe(rebaseUrls({root: '/web/assets/'}))
        .pipe(concat('libs.css'))
        .pipe(gulp.dest('./web/assets'));
});

var custom_assets = function() {
    gulp.src([
        './web/vendor/coffee/*.coffee',
        './web/vendor/coffee/*/*.coffee',
        './web/vendor/coffee/*/*/*.coffee',
        './web/vendor/coffee/*/*/*/*.coffee'
    ])
        .pipe(coffee({bare: true}))
        //.pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./web/assets'));

    gulp.src('./web/vendor/styles/*.less')
        .pipe(less())
        .pipe(cleanCSS())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./web/assets'));
};