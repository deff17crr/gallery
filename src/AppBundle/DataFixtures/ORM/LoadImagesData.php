<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadUserData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadImagesData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $albumData = [1 => 'First', 2 => 'Second', 3 => 'Third', 4 => 'Fourth', 5 => 'Fifth'];

        foreach ($albumData as $key => $value) {
            $album = new Album();
            $album->setTitle("$value album");
            $manager->persist($album);

            $dir = __DIR__."/{$key}_album/";
            $files = scandir($dir);

            foreach ($files as $file) {
                if (in_array($file, ['.', '..'])) {
                    continue;
                }

                $original = $dir.$file;

                $filename = tempnam(sys_get_temp_dir(), 'album_fixture');
                copy($original, $filename);

                $uploadedFile = new UploadedFile($filename, $file, null, null, null, true);

                $image = new Image();
                $image->setTitle("$value album, image $file");
                $image->setAlbum($album);
                $image->setImageFile($uploadedFile);

                $manager->persist($image);
            }

            $manager->flush();
        }
    }
}
