<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use AppBundle\Entity\ImageRepository;
use Doctrine\ORM\EntityManager;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Class ImageManager
 * @package AppBundle\Manager
 */
class ImageManager
{
    /** @var  EntityManager */
    private $em;

    /** @var  ImageRepository */
    private $repo;

    /** @var UploaderHelper */
    private $vichUploaderHelper;

    /** @var CacheManager */
    private $liipCacheManager;

    /** @var Paginator */
    private $knpPaginator;

    /**
     * ImageManager constructor.
     * @param EntityManager  $em
     * @param UploaderHelper $vichUploaderHelper
     * @param CacheManager   $liipCacheManager
     * @param Paginator      $knpPaginator
     */
    public function __construct(
        EntityManager $em,
        UploaderHelper $vichUploaderHelper,
        CacheManager $liipCacheManager,
        Paginator $knpPaginator
    ) {
        $this->em = $em;
        $this->repo = $em->getRepository('AppBundle:Image');
        $this->vichUploaderHelper = $vichUploaderHelper;
        $this->liipCacheManager = $liipCacheManager;
        $this->knpPaginator = $knpPaginator;
    }

    /**
     * @param integer $id
     * @return Image
     */
    public function get($id)
    {
        return $this->repo->find($id);
    }

    /**
     * @param Album   $album
     * @param integer $page
     * @return array
     */
    public function getPaginationData(Album $album, $page)
    {
        $query = $this->repo->getListQb($album);
        /** @var SlidingPagination $pagination */
        $pagination = $this->knpPaginator->paginate($query, $page);

        return [
            'total' => $pagination->getTotalItemCount(),
            'items' => $pagination->getItems(),
        ];
    }

    /**
     * @param Image $image
     * @return string
     */
    public function getPath(Image $image)
    {
        return $this->vichUploaderHelper->asset($image, 'imageFile');
    }

    /**
     * @param string $path
     * @param string $filter
     * @return string
     */
    public function getThumbnail($path, $filter = 'image_thumb')
    {
        return $this->liipCacheManager->getBrowserPath($path, $filter);
    }
}
