<?php

namespace AppBundle\Manager;

use AppBundle\Entity\AlbumRepository;
use Doctrine\ORM\EntityManager;

/**
 * Class AlbumManager
 */
class AlbumManager
{
    /** @var EntityManager */
    private $em;

    /** @var AlbumRepository */
    private $repo;

    /**
     * ImageManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repo = $em->getRepository('AppBundle:Album');
    }

    /**
     * @param integer $albumId
     * @return \AppBundle\Entity\Album
     */
    public function get($albumId)
    {
        return $this->repo->find($albumId);
    }

    /**
     * @return array
     */
    public function getListForIndex()
    {
        return $this->repo->getListForIndex();
    }
}
