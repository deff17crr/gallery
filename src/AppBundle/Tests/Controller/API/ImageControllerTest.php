<?php

namespace AppBundle\Tests\Controller\API;

use FOS\RestBundle\Util\Codes;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use AppBundle\DataFixtures\ORM\LoadImagesData;

/**
 * Class ImageControllerTest
 * @package AppBundle\Tests\Controller\API
 */
class ImageControllerTest extends WebTestCase
{
    /**
     * Prepare test data
     */
    protected function setUp()
    {
        $this->loadFixtures([LoadImagesData::class]);
    }

    /**
     * Test
     */
    public function testGetAlbumAction()
    {
        $randomAlmum = $this->getContainer()
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('AppBundle:Album')
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();

        $client = static::createClient();
        $client->request('GET', '/api/v1/albums/'.$randomAlmum->getId().'/images');
        $result = @json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue(isset($result['items']));

        $count = count($result['items']);
        if ($count == 10) {
            $client = static::createClient();
            $client->request('GET', '/api/v1/albums/'.$randomAlmum->getId().'/images?page=2');
            $result = @json_decode($client->getResponse()->getContent(), true);
            $this->assertEquals(count($result['items']), 10);
        }
    }

    /**
     * Test
     */
    public function testGetAlbumsAction()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/api/v1/albums');
        $result = @json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals(count($result), 5);

        foreach ($result as $album) {
            $this->assertTrue(count($album['images']) <= 10);
            $this->assertTrue(count($album['images']) >= 1);
        }

        $this->assertEquals(Codes::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
