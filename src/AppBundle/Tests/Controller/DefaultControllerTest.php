<?php

namespace AppBundle\Tests\Controller;

use FOS\RestBundle\Util\Codes;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 * @package AppBundle\Tests\Controller
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * Test index page status code
     */
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(Codes::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
