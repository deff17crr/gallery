<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class AlbumRepository
 * @package AppBundle\Entity
 */
class AlbumRepository extends EntityRepository
{
    /**
     * Get all albums with max 10 images
     *
     * @return array
     */
    public function getListForIndex()
    {
        return $this->createQueryBuilder('a')
//            ->leftJoin('a.images', 'i', 'WITH', '')
            ->getQuery()
            ->execute();
    }
}
