<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class ImageRepository
 * @package AppBundle\Entity
 */
class ImageRepository extends EntityRepository
{
    /**
     * @param Album|null $album
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQb(Album $album = null)
    {
        $qb = $this->createQueryBuilder('i');

        if ($album) {
            $qb->where('i.album = :album')
                ->setParameter('album', $album);
        }

        return $qb;
    }
}