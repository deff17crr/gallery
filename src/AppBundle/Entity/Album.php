<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AlbumRepository")
 * @ORM\Table(name="album")
 * @Serializer\ExclusionPolicy("none")
 */
class Album
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Image", mappedBy="album")
     * @Serializer\Exclude
     */
    private $images;

    /**
     * Album constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param Image $image
     */
    public function addImage(Image $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }
    }

    /**
     * @param Image $image
     */
    public function removeImage(Image $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->remove($image);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }


    /**
     * @return array
     *
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"index"})
     * @Serializer\SerializedName("images")
     */
    public function firstTenImages()
    {
        return $this->images->slice(0, 10);
    }
}
