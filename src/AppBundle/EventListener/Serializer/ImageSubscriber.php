<?php

namespace AppBundle\EventListener\Serializer;

use AppBundle\Entity\Image;
use AppBundle\Manager\ImageManager;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;

/**
 * Class ImageSubscriber
 * @package AppBundle\EventListener\Serializer
 */
class ImageSubscriber implements EventSubscriberInterface
{
    /** @var  ImageManager */
    private $imageManager;

    /**
     * ImageSubscriber constructor.
     * @param ImageManager $imageManager
     */
    public function __construct(ImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'method' => 'onImageSerialized',
                'class' => Image::class,
            ],
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onImageSerialized(ObjectEvent $event)
    {
        $visitor = $event->getVisitor();
        $path = $this->imageManager->getPath($event->getObject());

        $visitor->addData('path', $path);
        $visitor->addData('thumbnail', $this->imageManager->getThumbnail($path));
    }
}
