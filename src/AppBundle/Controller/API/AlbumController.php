<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Album;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AlbumController
 * @package AppBundle\Controller\API
 */
class AlbumController extends FOSRestController
{
    use ReturnControllerTrait;
    /**
     * @return \FOS\RestBundle\View\View
     */
    public function getAlbumsAction()
    {
        try {
            $albums = $this->getManager()->getListForIndex();

            return $this->response($albums, null, [], ['index', 'Default']);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @param Album $album
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAlbumAction(Album $album)
    {
        try {
            return $this->response($album, null, [], ['Default']);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @return \AppBundle\Manager\AlbumManager
     */
    private function getManager()
    {
        return $this->get('manager.album');
    }
}