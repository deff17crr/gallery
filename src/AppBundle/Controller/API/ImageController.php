<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\Album;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ImageController
 * @package AppBundle\Controller\API
 */
class ImageController extends FOSRestController
{
    use ReturnControllerTrait;

    /**
     * @param Album   $album
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAlbumImagesAction(Album $album, Request $request)
    {
        try {
            $page = intval($request->get('page', 1)) ?: 1;
            $data = $this->getManager()->getPaginationData($album, $page);

            return $this->response($data);
        } catch (\Exception $e) {
            return $this->response(['error' => $e->getMessage()], $e->getCode());
        }
    }



    /**
     * @return \AppBundle\Manager\ImageManager
     */
    private function getManager()
    {
        return $this->get('manager.image');
    }
}
